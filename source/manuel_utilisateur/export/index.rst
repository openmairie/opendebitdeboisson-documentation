.. _export:

######
Export
######

.. _nombre_licence_par_type:

**************************
Nombre de licence par type
**************************

Affiche le nombre de licence permanente par type.

* Nom de la reqmo : *statistiques_licences_par_type*
* Champs affichés :
    * type_licence : liste des types de licence
    * nombre_licence : nombre de licence par type
* Aucun choix de champ à afficher n’est proposé.
* Filtre : terme permanente
* Trie : par le libellé des types de licence

.. _licence_type_par_etablissement:

*********************************
Type de licence par établissement
*********************************

Affiche la liste des types de demande de licence par établissement.

* Nom de la reqmo : *statistiques_licence_type_par_etablissement*
* Champs affichés :
    * liste_etablissement : liste des établissements
    * type_licence_libelle : liste des types de licence
* Choix d'affichage sur les champs :
    * liste_etablissement
* Filtre :
    * terme permanente
    * terme temporaire
    * terme liée à un terrain de sport
* Trie : par le libellé des types de licence

.. _demande_licence_etablissement:

*********************************************************************************
Liste des demandes de licence avec toutes les informations de l'établissement lié
*********************************************************************************

Affiche la liste des demandes de licence avec l'établissement lié.

* Nom de la reqmo : *statistiques_demande_licence_etablissement*
* Champs affichés :
    * toutes les informations des demandes de licences
    * toutes les informations de l'établissement lié à la demande de licence
* Choix d'affichage sur les champs :
    * toutes les informations des demandes de licences
    * toutes les informations de l'établissement lié à la demande de licence
* Aucun filtre
* Trie : plusieurs choix, par défaut l'identifiant de la demande de licence
